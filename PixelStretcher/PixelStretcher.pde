int LINE_MIN_LENGTH = 4;
int LINE_MAX_LENGTH = 8;
int FREE_PIXEL_FIND_MAX_TRIES = 1000;
int LINES_PER_FRAME = 5000;
boolean RECURSE = true;
boolean CENTER_BIAS = true; //TODO: Smoothed center bias?

PImage image;
boolean[][] used;
int usedCount = 0;
int recursion = 0;


void setup() {
  size (1080, 720);
  image = loadImage("MarcusWebcam.jpg");
  used = new boolean[image.width][image.height];
  image(image, 0, 0);
}

void draw() {

  loadPixels();

  for (int i=0; i<LINES_PER_FRAME; i++)
    if (usedCount<used.length*used[0].length) 
      tryToStretchOnePixel();

  updatePixels();

  if (usedCount < pixels.length) {
    if (frameCount%50==0) {
      println(usedCount + " of " +pixels.length + " used. ("+nf(100.0*(float)usedCount/pixels.length, 1, 2)+"%)");
    }
  } else {

    if (!RECURSE) {
      saveImageAndExit();
    } else {
      saveImageAndRecurse();
    }
  }
}

void keyPressed() {
  if (key=='s') {
    saveImageAndExit();
  } else if (key=='r') {
    saveImageAndRecurse();
  }
}


void saveImage() {
  String rec = RECURSE ? (" recursion "+nf(recursion, 3)) : "";
  String bias = CENTER_BIAS ? " centerBias":"";
  String fileName = "output/PixelStretcher preview "+LINE_MIN_LENGTH+" "+LINE_MAX_LENGTH+rec+bias+".jpg";
  println("saving "+fileName);
  save(fileName);
}

void saveImageAndRecurse() {
  saveImage();
  recursion++;
  used = new boolean[image.width][image.height];
  usedCount=0;
}

void saveImageAndExit() {
  saveImage();
  exit();
}


void tryToStretchOnePixel() {

  Pixel pixel = getFreePixel();
  if (pixel!=null) {
    Pixel[] line = makeLineCoordinates(pixel);

    int col = pixels[pixel.y*width+pixel.x];
    for (Pixel pix : line) {
      pixels[pix.y*width+pix.x] = col;
    }
  }
}


Pixel getFreePixel() {
  Pixel out = null;
  int tries = 0;
  while (out==null && tries<FREE_PIXEL_FIND_MAX_TRIES) {
    tries++;
    int x = (int)random(image.width);
    int y = (int)random(image.height);
    if (!used[x][y]) {
      out = new Pixel(x, y);
      used[x][y] = true;
      usedCount++;
    }
  }
  return out;
}


Pixel[] makeLineCoordinates(Pixel centerPixel) {

  ArrayList<Pixel> outputLine = new ArrayList<Pixel>();

  boolean horizontal = random(1.0)<0.5;
  int lineLength = (int)random(LINE_MIN_LENGTH, LINE_MAX_LENGTH);

  int low, high;
  int direction = random(1.0)<0.5 ? -1 : 1;


  if (horizontal) {
    if (CENTER_BIAS) {
      direction = centerPixel.x < image.width/2 ? -1 : 1;
    }
    low = high = centerPixel.x;
    for (int i=0; i<lineLength; i++) {
      int nextPos = direction==-1 ? (low+direction) : (high+direction);
      low = min(low, nextPos);
      high = max(high, nextPos);
      if (low>=0 && high<image.width && !used[nextPos][centerPixel.y]) {
        Pixel linePixel = new Pixel(nextPos, centerPixel.y);
        used[nextPos][centerPixel.y] = true;
        usedCount++;
        outputLine.add(linePixel);
      } else {
        break; // stop making the line
      }
      if (!CENTER_BIAS) 
        direction = -direction;
    }
    //println(centerPixel+" -> "+low+".."+high+" / "+centerPixel.y);
  } else {
    if (CENTER_BIAS) {
      direction = centerPixel.y < image.height/2 ? -1 : 1;
    }
    low = high = centerPixel.y;
    for (int i=0; i<lineLength; i++) {
      int nextPos = direction==-1 ? (low+direction) : (high+direction);
      low = min(low, nextPos);
      high = max(high, nextPos);
      if (low>=0 && high<image.height && !used[centerPixel.x][nextPos]) {
        Pixel linePixel = new Pixel(centerPixel.x, nextPos);
        used[centerPixel.x][nextPos] = true;
        usedCount++;
        outputLine.add(linePixel);
      } else {
        break; // stop making the line
      }
      if (!CENTER_BIAS) 
        direction = -direction;
    }
  }

  return outputLine.toArray(new Pixel[0]);
}


class Pixel {
  int x, y;
  Pixel() {
  }
  Pixel(int x, int y) {
    this.x=x;
    this.y=y;
  }
  public String toString() { 
    return x+"/"+y;
  }
}
