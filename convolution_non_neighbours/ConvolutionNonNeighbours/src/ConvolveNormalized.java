import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class ConvolveNormalized {
	
	public static int[] makeStandardOffsets3x3(PImage img) {
		int width = img.width;
		return new int[] {
				-width-1, -width, -width+1,
				-1, 0, 1,
				width-1, width, width+1
		};
	}
	public static int[] makeRandomOffsets3x3(PImage img) {
		int size = img.width*img.height;
		int[] out = new int[9];
		for (int i=0;i<9;i++) {
			out[i] = (int)(Math.random()*size);
		}
		return out;
	}
	
	public static int getKernelDivider(int[] kernel) {
		int total = 0;
		for (int i=0;i<kernel.length;i++) {
			total += kernel[i];
		}
		return total;
	}
	
	
	
	
	PImage img;
	int[] r, g, b;
	int width, height, size;
	int[] tmpPixels;
	
	
	/**
	 * @param img
	 */
	public ConvolveNormalized(PImage img) {
		this.img = img;
		width = img.width;
		height = img.height;
		size = width*height;
		tmpPixels = new int[size];
		r = new int[size];
		g = new int[size];
		b = new int[size];
		
		//createRandomNeighbours();
	}
		
	public void doIt(int[] kernel, int[] offsets) {
		if (kernel.length!=offsets.length){
			throw new RuntimeException("Kernel and offsets are not equal in size!");
		}
		img.loadPixels();
		// copy img -> tmpPixels
		System.arraycopy(img.pixels, 0, tmpPixels, 0, width*height);
		
		int sourceColor;
		int sourceIndex;
		int kLength = kernel.length;
		int low = Integer.MAX_VALUE;
		int high = Integer.MIN_VALUE;
		for (int i=0;i<size;i++) {
			r[i]=0;
			g[i]=0;
			b[i]=0;
			for (int j=0;j<kLength;j++) {
				sourceIndex = i + offsets[j];
				if (sourceIndex<0) {
					sourceIndex += size;
				} else if (sourceIndex>=size) {
					sourceIndex -= size;
				}
				sourceColor = tmpPixels[sourceIndex];
				r[i] += kernel[j] * (sourceColor>>16&0xff);
				g[i] += kernel[j] * (sourceColor>>8&0xff);
				b[i] += kernel[j] * (sourceColor&0xff);
			}
			low = Math.min(low, Math.min(Math.min(r[i], g[i]), b[i]));
			high = Math.max(high, Math.max(Math.max(r[i], g[i]), b[i]));
			//r /= kerneldivider;
			//g /= kerneldivider;
			//b /= kerneldivider;
			//r = Math.max(0, Math.min(255, r));
			//g = Math.max(0, Math.min(255, g));
			//b = Math.max(0, Math.min(255, b));
			
			
			//img.pixels[i] = r<<16 | g<<8 | b;
		}
		
		// normalize
		float fac = 255.0f/(high-low);
		for (int i=0;i<size;i++) {
			r[i] = r[i]-low;
			r[i] *= fac;
			g[i] = g[i]-low;
			g[i] *= fac;
			b[i] = b[i]-low;
			b[i] *= fac;
			img.pixels[i] = r[i]<<16 | g[i]<<8 | b[i];
		}
		
		
		img.updatePixels();
	}
}
