import processing.core.PApplet;
import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class ConvNoNeighbours extends PApplet {
	
	public static void main(String[] args) {
		PApplet.main(new String[] {"ConvNoNeighbours"});
	}
	
	PImage img;
	//ConvolveNormalized convolve;
	Convolve convolve;
	
	int[] offsets;
	int[] kernel;
	float divider;
	
	public void setup() {
		size(1024,768);
		img = loadImage("Jura_Landscape_01_1024x768.jpg");//07_04_06_b_flat_landscape.jpg");
		
		//offsets = Convolve.makeRandomOffsets3x3(img);
		int kernelSize = 3;
		offsets = Convolve.makeStandardOffsets(img,kernelSize);
		kernel = Convolve.makeBlurKernel(kernelSize);
		kernel = new int[] {
				-1,1,1,
				-1,-2,1,
				-1,1,1
		};
		divider = 1;//Convolve.getKernelDivider(kernel);
		
		int maxOffset = 200;
		for (int i=0;i<offsets.length;i++) {
			offsets[i] += (int)((Math.random()*2-1)*maxOffset)
			+ ((int)((Math.random()*2-1)*maxOffset))*width;
		}
		
		//convolve = new ConvolveNormalized(img);
		convolve = new Convolve(img);
		//Convolve.makeBlurKernel(3);
		
	}
	
	public void keyPressed() {
		convolve.doIt(kernel, divider, offsets);
		/*convolve.doIt(
				new int[] {
						-1,1,1,
						-1,-2,1,
						-1,1,1,
				},
				1,
				
				new int[] {1,2,1,2,4,2,1,2,1},
				16,
				offsets);*/
		println("convolved");
	}
	
	public void draw() {
		image(img,0,0);
	}
}
