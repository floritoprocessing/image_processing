import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Convolve {
	
	public static int[] makeStandardOffsets(PImage img, int size) {
		int width = img.width;
		int[] out = new int[size*size];
		int low = -(size-1)/2;
		int high = low+size-1;
		int index = 0;
		for (int y=low;y<=high;y++) {
			for (int x=low;x<=high;x++) {
				out[index] = y*width+x;
				index++;
			}
		}
		return out;
	}
	
	public static int[] makeStandardOffsets3x3(PImage img) {
		int width = img.width;
		return new int[] {
				-width-1, -width, -width+1,
				-1, 0, 1,
				width-1, width, width+1
		};
	}
	public static int[] makeRandomOffsets3x3(PImage img) {
		int size = img.width*img.height;
		int[] out = new int[9];
		for (int i=0;i<9;i++) {
			out[i] = (int)(Math.random()*size);
		}
		return out;
	}
	
	public static int getKernelDivider(int[] kernel) {
		int total = 0;
		for (int i=0;i<kernel.length;i++) {
			total += kernel[i];
		}
		return total;
	}
	
	public static int[] makeBlurKernel(int size) {
		float[] tmp = new float[size*size];
		int[] out = new int[size*size];
		int low = -(size-1)/2;
		int high = low+size-1;
		int index = 0;
		float maxDistance = (float)Math.sqrt(high*high+high*high);
		float minValueAboveZero = Float.MAX_VALUE;
		for (int y=low;y<=high;y++) {
			for (int x=low;x<=high;x++) {
				float dToCenter = (float)Math.sqrt(x*x+y*y);
				float dToEdge = maxDistance - dToCenter;
				dToEdge /= maxDistance;
				if (dToEdge!=0) {
					minValueAboveZero = Math.min(minValueAboveZero, dToEdge);
				}
				//System.out.println(x+"/"+y+": "+dToEdge);
				tmp[index] = dToEdge;
				index++;
			}
		}
		
		for (int i=0;i<out.length;i++) {
			out[i] = (int)(tmp[i] / minValueAboveZero);
			//System.out.println(out[i]);
		}
		return out;
	}
	
	
	
	PImage img;
	int width, height, size;
	int[] tmpPixels;
	
	
	/**
	 * @param img
	 */
	public Convolve(PImage img) {
		this.img = img;
		width = img.width;
		height = img.height;
		size = width*height;
		tmpPixels = new int[size];
		
		//createRandomNeighbours();
	}
		
	public void doIt(int[] kernel, float kerneldivider, int[] offsets) {
		if (kernel.length!=offsets.length){
			throw new RuntimeException("Kernel and offsets are not equal in size!");
		}
		img.loadPixels();
		// copy img -> tmpPixels
		System.arraycopy(img.pixels, 0, tmpPixels, 0, width*height);
		
		int sourceColor, r,g,b;
		int sourceIndex;
		int kLength = kernel.length;
		for (int i=0;i<size;i++) {
			r=0;
			g=0;
			b=0;
			for (int j=0;j<kLength;j++) {
				sourceIndex = i + offsets[j];
				if (sourceIndex<0) {
					sourceIndex += size;
				} else if (sourceIndex>=size) {
					sourceIndex -= size;
				}
				sourceColor = tmpPixels[sourceIndex];
				r += kernel[j] * (sourceColor>>16&0xff);
				g += kernel[j] * (sourceColor>>8&0xff);
				b += kernel[j] * (sourceColor&0xff);
			}
			r /= kerneldivider;
			g /= kerneldivider;
			b /= kerneldivider;
			r = Math.max(0, Math.min(255, r));
			g = Math.max(0, Math.min(255, g));
			b = Math.max(0, Math.min(255, b));
			
			
			img.pixels[i] = r<<16 | g<<8 | b;
		}
		img.updatePixels();
	}
}
