color black=#000000;
ImageDistorter[] imgdist; int nr_imgdist; int maxnr_imgdist;

PImage img; int image_w,image_h;

void setup() {
  size(600,600,P3D); noStroke(); noFill(); ellipseMode(DIAMETER);
  img=loadImage("SheepHead.jpg"); image_w=200; image_h=200;
  img.loadPixels();
  nr_imgdist=0; maxnr_imgdist=10;
  imgdist=new ImageDistorter[maxnr_imgdist];
  for (int i=0;i<maxnr_imgdist;i++) {
    imgdist[i]=new ImageDistorter(200,200); 
  }
  imgdist[0].active=true;
}

void draw() {
  background(black);
  translate(200,200);
  float pw=1.0, pwh=pw/2.0;
  for (int y=0;y<image_h;y++) {
    for (int x=0;x<image_w;x++) {
      int i=y*image_w+x;
      float sx=x*pw;
      float sy=y*pw;
      float sz=-50;
      for (int j=0;j<maxnr_imgdist;j++) {
        imgdist[j].update();
        sz+=imgdist[j].z[i];
      }
      stroke(img.pixels[i]);
      point(sx,sy,sz);
    }
  }
}

void mousePressed() {
  int i=0; boolean found_none=true;
//  while ((i<maxnr_imgdist)&&found_none) {
//    if (!imgdist[i].active) {
//      found_none=false;
//      imgdist[0].active=true;
//    }
//    i++;
//  }
//  imgdist[0].active=true;
}

class ImageDistorter {
  int w,h;
  float cx, cy, mxd;
  float[] z;
  float s_amp, amp, decay;
  float r=0.0, mr;            // distance wave from center
  boolean active;
  
  ImageDistorter(int iw, int ih) {  // image width/height
    w=iw; h=ih; mxd=sqrt(iw*iw+ih*ih); mr=mxd;
    z=new float[ih*iw];
    s_amp=100; decay=1.0;
    for (int i=0;i<ih*iw;i++) {z[i]=0.0;}
    cx=iw*.5; cy=ih*.5;  // press point is center of image
    active=false;
  }
  
  void update() {
    if (active) {
      r+=5.0; decay*=0.95;
      for (int y=0;y<h;y++) {
        for (int x=0;x<w;x++) {
          int i=y*w+x;
          float d=dist(x,y,cx,cy);
          if (d<r) {
            amp=decay*(s_amp-s_amp*d/mxd);
            z[i]=amp*sin(PI*(d-r)/30.0);
          } else {z[i]=0.0;}
        }
      }
      if (decay<0.005) {active=false;}
    }
  }
}
