//My First Edge Detection
//http://www.pages.drexel.edu/~weg22/edge.html

String IMAGE_NAME="car_320.jpg";//"dalay-lama-150.jpg";//"car_640.jpg";//
int MODE=0;  // 0: show source - 1: show result

EdgeImage edgeImage;


void setup() {
  edgeImage=new EdgeImage(IMAGE_NAME);
  size(320,240);
  edgeImage.setThreshold(0);
  edgeImage.calcEdge();
  edgeImage.showBoth();
}

void draw() {
  if (mousePressed&&pmouseX!=mouseX) {
    edgeImage.setThreshold(constrain(edgeImage.getThreshold()+pmouseY-mouseY,0,255));
    print("thresh: "+edgeImage.getThreshold()+"\t");
    edgeImage.calcEdge();
    //edgeImage.showOutput();
    edgeImage.showBoth();
  }
}
