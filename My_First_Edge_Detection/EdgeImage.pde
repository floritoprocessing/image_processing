class EdgeImage  {
  PImage input, output;
  int WIDTH=0, HEIGHT=0;
  int THRESHOLD=0;
  
  int[][] GX={ 
    {
      -1,0,1    } 
    , {
      -2,0,2    } 
    , {
      -1,0,1    } 
  };
  int[][] GY={ 
    {
      1,2,1    } 
    , {
      0,0,0    } 
    , {
      -1,-2,-1    } 
  };

  EdgeImage(PImage in) {
    input=in;
    WIDTH=input.width;
    HEIGHT=input.height;
    output=new PImage(WIDTH,HEIGHT);
  }

  EdgeImage(String s) {
    input=loadImage(s);
    WIDTH=input.width;
    HEIGHT=input.height;
    output=new PImage(WIDTH,HEIGHT);
  }

  void setImage(PImage in) {
    input=in;
    WIDTH=input.width;
    HEIGHT=input.height;
    output=new PImage(WIDTH,HEIGHT);
  }

  void setImage(String s) {
    input=loadImage(s);
    WIDTH=input.width;
    HEIGHT=input.height;
    output=new PImage(WIDTH,HEIGHT);
  }

  void setThreshold(int t) {
    THRESHOLD=t;
  }
  
  int getThreshold() {
    return THRESHOLD;
  }
  
  void calcEdge() {
    // analyse this
    float ms=millis();
    float SUM=0, SUMX=0, SUMY=0;
    for (int y=0;y<HEIGHT;y++) {
      for (int x=0;x<WIDTH;x++) {
        SUMX=0;
        SUMY=0;
        // edges:
        if (x==0||x==WIDTH-1) SUM=0;
        else if (y==0||y==HEIGHT-1) SUM=0;
        // all other cases:
        else {
          // x and y gradient approximation
          for (int i=-1;i<=1;i++) {
            for (int j=-1;j<=1;j++) {
              SUMX+=GX[i+1][j+1]*brightness(input.get(x+i,y+j));
              SUMY+=GY[i+1][j+1]*brightness(input.get(x+i,y+j));
            }
          }
          // gradient magnitude approximation
          SUM=abs(SUMX)+abs(SUMY);
          //SUM=sqrt(SUMX*SUMX+SUMY*SUMY);
          if (SUM<THRESHOLD) SUM=0;
          if (SUM>255) SUM=255;
        }
        output.pixels[y*WIDTH+x]=color(SUM);
      }
    }
    output.updatePixels();
    println("calc: "+(millis()-ms)+" ms");
  }

  void showInput() {
    image(input,0,0);
  }

  void showOutput() {
    image(output,0,0);
  }
  
  void showBoth() {
    for (int x=0;x<WIDTH;x++) {
      for (int y=0;y<HEIGHT;y++) {
        set(x,y,colAdd(input.pixels[y*WIDTH+x],output.pixels[y*WIDTH+x]));
      }
    }
  }
  
  color colAdd(color a, color b) {
    return color((a>>16&0xff)+(b>>16&0xff),(a>>8&0xff)+(b>>8&0xff),(a&0xff)+(b&0xff));
  }
}
