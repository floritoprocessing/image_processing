package graf.pixeleffects;

import processing.core.PGraphics;

public class ImageProc {

	public static void threshold(PGraphics source, PGraphics target, int low, int high, boolean useLuminance) {
		// Luminance = (0.2126*R) + (0.7152*G) + (0.0722*B)
		// 128 (<<7) = 26			92			 10
		// 256		 = 52			184			 20
		source.loadPixels();
		int r, g, b, v; 
		float luminance;
		for (int i=0;i<source.pixels.length;i++) {
			r = source.pixels[i]>>16&0xff;
			g = source.pixels[i]>>8&0xff;
			b = source.pixels[i]&0xff;
			if (useLuminance) {
				luminance = (0.2126f*r) + (0.7152f*g) + (0.0722f*b);
			} else {
				luminance = (0.3333f*r) + (0.3333f*g) + (0.3333f*b);
			}
			//v = (int)luminance;
			v = (int)interpolate(luminance, low, high, 0, 255, true);
			target.pixels[i] = 0xff<<24|v<<16|v<<8|v;
		}
		target.updatePixels();
	}
	
	private static float interpolate(float in, float inLimLow, float inLimHigh, float outLimLow, float outLimHigh, boolean clip) {
		float percentage = (in-inLimLow)/(inLimHigh-inLimLow);
		if (clip) {
			percentage = Math.min(1,Math.max(0,percentage));
		}
		return outLimLow + percentage*(outLimHigh-outLimLow);
	}

}
