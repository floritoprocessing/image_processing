package graf.pixeleffects;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class PixelEffects extends PApplet {

	private static final long serialVersionUID = -7427890472549953951L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.pixeleffects.PixelEffects"});
	}
	
	PGraphics orig;
	PGraphics brightImage;
	int imageToShow = 0;
	
	public void settings() {
		size(800,600,P3D);
	}
	
	public void setup() {
		
		orig = createGraphics(width, height, P3D);
		orig.beginDraw();
		orig.background(0);
//		try {
			PImage img = loadImage("images/Herschel_600h.jpg");
			float aspRat = (float)img.width/img.height;
			float appAspRat = (float)width/height;
			if (aspRat>appAspRat) {
				// scale to fit width
				float w = width;
				float h = width/aspRat;
				orig.image(img,(width-w)/2,(height-h)/2,w,h);
			} else {
				// scale to fit height
				float w = height*aspRat;
				float h = height;
				orig.image(img,(width-w)/2,(height-h)/2,w,h);
			}
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		orig.endDraw();
		orig.updatePixels();
		
		brightImage = createGraphics(width, height, P3D);
		brightImage.loadPixels();
		ImageProc.threshold(orig,brightImage,100,200,true);
	}
	
	public void mousePressed() {
		imageToShow++;
	}
	
	public void draw() {
		switch (imageToShow) {
		case 0: image(orig,0,0); break;
		case 1: image(brightImage,0,0); break;
		case 2: imageToShow = 0;
		}
	}
}
