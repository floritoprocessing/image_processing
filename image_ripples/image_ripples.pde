color black=#000000;
PImage img; int image_w,image_h;
float av_brightness;
DistImage distimage;

void setup() {
  size(200,200); noStroke(); noFill(); ellipseMode(DIAMETER);
  img=loadImage("SheepHead.jpg"); image_w=200; image_h=200;
  av_brightness=getAverageBrightness(img);
  distimage=new DistImage(img);
}

void draw() {
  background(black);
  image(img,50,0,img.width/2.0,img.height/2.0);
  distimage.update();
  showDistImage(50,100,0.5,av_brightness);
}

class DistImage {
  color[] c;
  int w,h;
  float[] x,y, sx;
  float[] freq, amp, phase;
  float ti;
  
  DistImage(PImage in_img) {
    ti=0.0;
    w=in_img.width; h=in_img.height;
    int maxpix=h*w;
    c=new color[maxpix];
    x=new float[maxpix]; y=new float[maxpix]; sx=new float[maxpix];
    amp=new float[maxpix]; phase=new float[maxpix];
    
    for (int iy=0;iy<h;iy++) {
      for (int ix=0;ix<w;ix++) {
        int i=iy*w+ix;
       
        c[i]=in_img.pixels[i];
        x[i]=ix;
        sx[i]=w/2.0 + (1+0.5*(h-iy)/((float)h))*(ix-w/2.0);
        y[i]=h-iy;
        
        amp[i]=2+20*(h-iy)/((float)h);
        phase[i]=10*TWO_PI*pow(iy/(float)h,2);
      }
    }
  }
  
  void update() {
    ti+=0.3;
    for (int iy=0;iy<h;iy++) {
      for (int ix=0;ix<w;ix++) {
        int i=iy*w+ix;
        x[i]=sx[i]+amp[i]*sin(ti + phase[i]);
      }
    }
  }
  
}

void showDistImage(int xo, int yo, float mainScale, float br) {
  for (int y=0;y<image_h;y++) {
    for (int x=0;x<image_w;x++) {
      int i=y*image_w+x;
      color c=distimage.c[i];
      if (brightness(c)<br) {
        set(int(mainScale*distimage.x[i])+xo,int(mainScale*distimage.y[i])+yo,c);
      }
    }
  }
}

float getAverageBrightness(PImage img) {
  int n=img.width*img.height;
  float br=0.0;
  for (int i=0;i<n;i++) {
    br+=brightness(img.pixels[i]);
  } 
  br/=(float)n;
  return br;
}
