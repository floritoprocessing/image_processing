PImage img;
color[][] img1,img2;

//float val=1.0/9.0;
//float[][] blurArray={{val,val,val},{val,val,val},{val,val,val}};

int nb=5; //neighbour
float partial=1.0/9.0;

void setup() {
  size(400,400);
  img=loadImage("image2.jpg");
  imageMode(DIAMETER); 
  image(img,width/2,height/2,width/2,height/2);
  
  img1=new color[width+1][height+1];
  img2=new color[width+1][height+1];
  loadPixels();
  for (int y=1;y<=height;y++) {
    for (int x=1;x<=width;x++) {
      img1[x][y]=pixels[(y-1)*width+(x-1)];
    }
  }
}

void draw() {
  partial=1.0/float((2*nb+1)*(2*nb+1));

  for (int y=int(0.25*height);y<=int(0.75*height);y++) {
    for (int x=int(0.25*width);x<=int(0.75*width);x++) {
      //int index=(y+yo-1)*width+(x+yo-1);
      
      color pixCol=color(0,0,0);
      float r=0,g=0,b=0;
      
      for (int xo=-nb;xo<=nb;xo++) {
        for (int yo=-nb;yo<=nb;yo++) {
          int xp=x+xo, yp=y+yo;
          if (xp<1) {xp+=width;} if (xp>width) {xp-=width;}
          if (yp<1) {yp+=height;} if (yp>height) {yp-=height;}
          int index=(yp-1)*width+(xp-1);
          color pixel = pixels[index];
          r+=partial*red(pixel);
          g+=partial*green(pixel);
          b+=partial*blue(pixel);
        }
      }
      img1[x][y]=color(r,g,b);
      
      
    }
  }
  img.updatePixels();

  for (int y=int(0.25*height);y<=int(0.75*height);y++) {
    for (int x=int(0.25*width);x<=int(0.75*width);x++) {
      pixels[(y-1)*width+(x-1)]=img1[x][y];
    }
  }
  updatePixels();
}
