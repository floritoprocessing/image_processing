package blurring;

import processing.core.PImage;

/**
 * Simple blur using one horizontal and one vertical blur<br>
 * 
 * @author mgraf
 *
 */
public class SimpleBlurGaussian {
	
	public static void filter(PImage srcAndDst, float radius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurAndSwapAxes(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, radius);
		blurAndSwapAxes(dst,srcAndDst.pixels,srcAndDst.height, srcAndDst.width, radius);
	}
	
	private static void blurAndSwapAxes(int[] in, int[] out, int width, int height, float radiusFloat) {
		
		float[] kernel = makeKernel(radiusFloat);
		int cols = kernel.length;
		int cols2 = cols/2;
		int radius = cols2;
		
		int wm1 = width-1;
		int indexX=0;
		int indexY=0;
		int rgb;
		int outIndex=0;
		int a, r, g, b;
		for (int y=0;y<height;y++) {
			outIndex = y;
			for (int x=0;x<width;x++) {
				float ta=0, tr=0, tg=0, tb=0;
				for (int xo=-radius;xo<=radius;xo++) {
					float f = kernel[cols2+xo];
					if (f!=0) {
						indexX = x+xo;
						if (indexX<0) {
							indexX = 0;
						} else if (indexX>wm1) {
							indexX = wm1;
						}
						rgb = in[indexY+indexX];
						ta += f * ((rgb>>24)&0xff);
						tr += f * ((rgb>>16)&0xff);
						tg += f * ((rgb>>8)&0xff);
						tb += f * (rgb&0xff);
					}
				}
				a = (int)(ta+0.5);
				if (a<0) a=0; else if (a>255) a=255;
				r = (int)(tr+0.5);
				if (r<0) r=0; else if (r>255) r=255;
				g = (int)(tg+0.5);
				if (g<0) g=0; else if (g>255) g=255;
				b = (int)(tb+0.5);
				if (b<0) b=0; else if (b>255) b=255;
				out[outIndex] = a<<24 | r<<16 | g<<8 | b;
				outIndex += height;
			}
			indexY += width;
		}	
	}
	
	
	public static float[] makeKernel(float radius) {
		int r = (int)Math.ceil(radius);
		int rows = r*2+1;
		float[] matrix = new float[rows];
		float sigma = radius/3;
		float sigma22 = 2*sigma*sigma;
		float sigmaPi2 = 2*(float)Math.PI*sigma;
		float sqrtSigmaPi2 = (float)Math.sqrt(sigmaPi2);
		float radius2 = radius*radius;
		float total = 0;
		int index = 0;
		for (int row = -r; row <= r; row++) {
			float distance = row*row;
			if (distance > radius2) {
				matrix[index] = 0;
			} else {
				matrix[index] = (float)Math.exp(-(distance)/sigma22) / sqrtSigmaPi2;
			}
			total += matrix[index];
			index++;
		}
		for (int i = 0; i < rows; i++) {
			matrix[i] /= total;
		}
		return matrix;
	}
}
