package blurring;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.net.URISyntaxException;
import java.util.ArrayList;

import processing.core.*;

/**
 * Based on the article <a href="http://www.jhlabs.com/ip/blurring.html">Blurring for Beginners</a>
 * by 
 * @author mgraf
 *
 */
public class BlurringForBeginners extends PApplet {

	private static final long serialVersionUID = 5051580592876615364L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"blurring.BlurringForBeginners"});
	}
	
	String[] images = new String[] {
			"citylights1","citylights2","citylights3",
			"Herschel_600h",
			"lights","lights2"};
	int imageIndex = 0;
	
	PImage original;
	FXImage fxImage;
	
	long ti = millis();
	private boolean showOrig;
	private Path path;
	
	private ArrayList<Point2D.Float> drawPath = new ArrayList<Point2D.Float>();
	private boolean drawPathLined = false;
	private float drawPathFullDetail = 0.7f;
	private float gain = 3;
	private boolean drawNew;
	
	public void settings() {
		size(848,600,P3D);
	}
	public void setup() {
		
		
		imageIndex = (int)random(images.length);
		loadOriginal();
		
		fxImage = new FXImage(original.width,original.height);
		fxImage.loadPixels();
		fxImage.copy(original, 0, 0, fxImage.width, fxImage.height, 0, 0, fxImage.width, fxImage.height);
		
		SimpleConvolutionPath.DEBUG = true;
		SimpleConvolutionPathSubpixel.DEBUG = true;		
	}
	
	private void loadOriginal() {
//		try {
			print("Loading "+images[imageIndex]+"... ");
			String name = "./images/"+images[imageIndex]+".jpg";
			original = loadImage(name);
			original.loadPixels();
			println("done.");
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
	}
	
	private Path createRandomPath() {
		int length = 60;
		Point2D.Float[] points = new Point2D.Float[length];
		float spd=1;
		float rd=random(TWO_PI);
		float x=0, y=0;
		
		for (int i=0;i<length;i++) {
			spd += random(-0.2f,0.2f);
			rd += random(0.05f,0.5f) * (Math.random()<0.5?-1:1);
			x += spd*Math.cos(rd);
			y += spd*Math.sin(rd);
			
			points[i] = new Point2D.Float(x,y);
		}
		
		Path out = new Path(points);
		centerPath(out);
		return out;
	}
	
	private void centerPath(Path path) {
		float minX = Float.MAX_VALUE;
		float minY = Float.MAX_VALUE;;
		float maxX = Float.MIN_VALUE;
		float maxY = Float.MIN_VALUE;
		for (int i=0;i<path.size();i++) {
			minX = Math.min(minX,path.getPoint(i).x);
			minY = Math.min(minY,path.getPoint(i).y);
			maxX = Math.max(maxX,path.getPoint(i).x);
			maxY = Math.max(maxY,path.getPoint(i).y);
		}
		float offx = (maxX-minX)/2;
		float offy = (maxY-minY)/2;
		for (int i=0;i<path.size();i++) {
			path.getPoint(i).x -= (minX+offx);
			path.getPoint(i).y -= (minY+offy);
		}
	}
	
	public void keyPressed() {
		if (key==CODED) {
			if (keyCode==UP) {
				gain += 0.25f;
				println("gain: "+gain+", hit [SPACE] to redraw");
			} else if (keyCode==DOWN) {
				gain -= 0.25f;
				if (gain<0.25f) gain=0.25f;
				println("gain: "+gain+", hit [SPACE] to redraw");
			} else if (keyCode==RIGHT) {
				imageIndex++;
				if (imageIndex==images.length) {
					imageIndex=0;
				}
				loadOriginal();
				println("hit [SPACE] to redraw");
			} else if (keyCode==LEFT) {
				imageIndex--;
				if (imageIndex==-1) {
					imageIndex=images.length-1;
				}
				loadOriginal();
				println("hit [SPACE] to redraw");
			}
		}
		
		else {
			if (key==' ') {
				drawNew=true;
			}
		}
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) {
			drawPath = new ArrayList<Point2D.Float>();
			drawPath.add(new Point2D.Float(mouseX,mouseY));
		} else {
			drawPathLined = !drawPathLined;
			//drawNew = true;
		}
	}
	
	public void mouseDragged() {
		if (drawPath!=null) {
			if (drawPathLined) {
				Point2D.Float p0 = drawPath.get(drawPath.size()-1);
				Point2D.Float p1 = new Point2D.Float(mouseX,mouseY);
				float dx = p1.x-p0.x;
				float dy = p1.y-p0.y;
				float d = sqrt(dx*dx+dy*dy);
				float step = drawPathFullDetail;
				for (float p=step;p<d;p++) {
					float percentage = p/d;
					Point2D.Float point = new Point2D.Float(p0.x+percentage*dx, p0.y+percentage*dy);
					drawPath.add(point);
				}
				drawPath.add(p1);
			} else {
				drawPath.add(new Point2D.Float(mouseX,mouseY));
			}
		}
	}
	
	public void mouseReleased() {
		if (mouseButton==LEFT) {
			Point2D.Float[] points = new Point2D.Float[drawPath.size()];
			points = drawPath.toArray(points);
			path = new Path(points);
			centerPathTo(path,path.getPoint(0));
			invertPath(path);
			drawPath = null;
			drawNew = true;
		}
	}
	
	/**
	 * @param path2
	 * @param float1
	 */
	private void centerPathTo(Path path, Point2D.Float point) {
		float x = point.x;
		float y = point.y;
		for (int i=0;i<path.size();i++) {
			path.getPoint(i).x -= x;
			path.getPoint(i).y -= y;
		}
	}

	/**
	 * @param path2
	 */
	private void invertPath(Path path) {
		for (int i=0;i<path.size();i++) {
			path.getPoint(i).x *= -1;
			path.getPoint(i).y *= -1;
		}
	}

	public void draw() {
		background(0,0,15);
		
				
		if (drawNew) {
			fxImage.copy(original, 0, 0, fxImage.width, fxImage.height, 0, 0, fxImage.width, fxImage.height);
			ti = millis();
			fxImage.filter(FXImage.BLUR_SIMPLE_PATH,path,gain);
			System.out.println(millis()-ti);			
			drawNew = false;
		}
		
		image(fxImage,0,0);
		
		if (showOrig) {
			float scale = 0.4f;
			int wid = (int)(original.width*scale);
			int hei = (int)(original.height*scale);
			blend(original,0,0,original.width,original.height,(width-wid)/2,height-hei,wid,hei,NORMAL);
		}
		
		if (drawPath!=null && drawPath.size()>0) {
			stroke(255,0,0);
			noFill();
			beginShape(POINTS);
			for (Point2D.Float p:drawPath) {
				vertex(p.x,p.y);
			}
			endShape();
		}
		
		//noLoop();
		//println(ti);
	}
}
