package blurring;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Path kernel. Pixel collection
 * @author mgraf
 *
 */
public class Path {
	
	private Point2D.Float[] point;
		
	public Path(Point2D.Float[] point) {
		this.point = point;
	}
	
	public int size() {
		return point.length;
	}

	public Point2D.Float getPoint(int i) {
		return point[i];
	}
	
	public String toString() {
		String out="";
		for (Point2D.Float p:point) {
			out += p.toString() + "\r\n";
		}
		return out;
	}
}
