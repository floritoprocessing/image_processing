package blurring;

import processing.core.PImage;

public class SimpleHBlur {

	public static void filter(PImage srcAndDst, int radius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurHorizontal(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, radius);
		srcAndDst.pixels = dst;
	}

	public static void blurHorizontal(int[] in, int[] out, int width, int height, int radius) {
		int tableSize = 2*radius+1;
		float kernFac = 1.0f/tableSize;
		int wm1 = width-1;
		int indexX=0;
		int indexY=0;
		int rgb;
		for (int y=0;y<height;y++) {
			indexY = y*width;
			
			for (int x=0;x<width;x++) {
				int ta=0, tr=0, tg=0, tb=0;
				for (int xo=-radius;xo<=radius;xo++) {
					indexX = x+xo;
					if (indexX<0) {
						indexX = 0;
					} else if (indexX>wm1) {
						indexX = wm1;
					}
					rgb = in[indexY+indexX];
					ta += (rgb>>24)&0xff;
					tr += (rgb>>16)&0xff;
					tg += (rgb>>8)&0xff;
					tb += rgb&0xff;
				}
				out[indexY+x] = ((int)(ta*kernFac))<<24 | ((int)(tr*kernFac))<<16 | ((int)(tg*kernFac))<<8 | ((int)(tb*kernFac));
			}	
		}	
	}
	
}
