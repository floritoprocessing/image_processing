package blurring;

import java.awt.geom.Point2D;

import processing.core.PImage;

public class SimpleConvolutionPath {

	public static boolean DEBUG;
	
	public static void filter(PImage srcAndDst, float distance, float angle) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		
		int amount = (int)Math.ceil(distance);
		Point2D.Float[] points = new Point2D.Float[amount];
		float cos = (float)Math.cos(angle);
		float sin = (float)Math.sin(angle);
		for (int i=0;i<amount;i++) {
			float r = distance*i/(amount-1) - distance/2;
			points[i] = new Point2D.Float(r*cos,r*sin); 
		}
		Path path = new Path(points);
		
		convolve(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, path, 1);//radius);
		srcAndDst.pixels = dst;
	}
	
	public static void filter(PImage srcAndDst, Path path, float gain) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		convolve(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, path, gain);//radius);
		srcAndDst.pixels = dst;
	}
	
	public static void filter(PImage srcAndDst, Path path) {
		filter(srcAndDst, path, 1);
	}

	public static void convolve(int[] in, int[] out, int width, int height, Path path, float gain) {
		
		int wm1 = width-1;
		int hm1 = height-1;
		int rowIndex=0;
		int lastPercentage=0;
		for (int y=0;y<height;y++) {
			rowIndex = y*width;
			
			for (int x=0;x<width;x++) {
				int ta=0, tr=0, tg=0, tb=0;
				int total = 0;
				
				for (int i=0;i<path.size();i++) {
					
					int xo = (int)path.getPoint(i).x;
					int yo = (int)path.getPoint(i).y;
					int px = x+xo;
					int py = y+yo;
					
					if (px<0||px>wm1||py<0||py>hm1) {
						//continue;
					} else {
						int rgb = in[py*width + px];
						ta += (rgb>>24)&0xff;
						tr += (rgb>>16)&0xff;
						tg += (rgb>>8)&0xff;
						tb += rgb&0xff;
						total ++;
					}
					
					
				}
				
				if (total>0) {
					ta /= total;
					tr *= gain/total;
					tg *= gain/total;
					tb *= gain/total;
					if (tr>255) tr=255;
					if (tg>255) tg=255;
					if (tb>255) tb=255;
					out[rowIndex+x] = ta<<24 | tr<<16 | tg<<8 | tb;
				} else {
					out[rowIndex+x] = in[rowIndex+x];
				}
			}
			
			if (DEBUG) {
				int percentage = (100*y/height);
				if (percentage%5==0 && percentage!=lastPercentage) {
					System.out.print(percentage+"..");
					lastPercentage=percentage;
				}
			}
			
		}
		System.out.println();
	}
	
}
