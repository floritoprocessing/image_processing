package blurring;

public class ImageMath {

	public static int clamp(int x, int xMin, int xMax) {
		if (x<xMin) return xMin;
		else if (x>xMax) return xMax;
		else return x;
	}

}
