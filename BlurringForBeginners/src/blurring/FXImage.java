package blurring;

import java.awt.Image;

import processing.core.PImage;

public class FXImage extends PImage {

	private static final int[] origFilters = {THRESHOLD, GRAY, INVERT, POSTERIZE, BLUR, OPAQUE, ERODE, DILATE};
	// 16, 12, 13, 15, 11, 14, 17 18

	/**
	 * Box blur using a very basic convolution kernel (slow!)
	 */
	public static final int blurConvolute = 100;
	/**
	 * Horizontal blur
	 */
	public static final int blurHSimple = 101;
	/**
	 * Box blur using 2 horizontal blurs that swap axes after each other
	 */
	public static final int BLUR_SIMPLE = 102;
	/**
	 * Box blur like {@link #BLUR_SIMPLE}, but with all averaging divisions pre-calculated
	 */
	public static final int BLUR_SIMPLE_PREDIV = 103;
	/**
	 * Box blur like {@link #BLUR_SIMPLE_PREDIV}, but with premultiplied alpha
	 */
	public static final int BLUR_SIMPLE_PREMULT = 104;
	/**
	 * Gaussian blur
	 */
	public static final int BLUR_SIMPLE_GAUSSIAN = 105;
	/**
	 * Gaussian blur with premultiplied alpha
	 */
	public static final int BLUR_SIMPLE_GAUSSIAN_PREMULT = 106;
	/**
	 * Box blur like {@link #BLUR_SIMPLE}, but with a sliding frame adding and subtracting new pixels
	 */
	public static final int BLUR_SIMPLE_SLIDING = 107;
	/**
	 * Box blur like {@link #BLUR_SIMPLE_SLIDING}, but now with all averaging divisions pre-calculated
	 */
	public static final int BLUR_SIMPLE_SLIDING_PREDIV = 108;
	/**
	 * Box blur like {@link #BLUR_SIMPLE_SLIDING_PREDIV}, but now with float radii. Takes about 2.5x as long as {@link #BLUR_SIMPLE_SLIDING_PREDIV}
	 */
	public static final int BLUR_SIMPLE3_SLIDING_PREDIV_SUBPIXEL = 109;
	
	/**
	 * Convolution along a path
	 */
	public static final int BLUR_SIMPLE_PATH = 110;

	/**
	 * Convolution along a path with subpixel
	 */
	public static final int BLUR_SIMPLE_PATH_SUBPIXEL = 111;
	
	public FXImage() {
		super();
	}

	public FXImage(Image arg0) {
		super(arg0);
	}

	public FXImage(int arg0, int arg1, int arg2) {
		super(arg0, arg1, arg2);
	}

	public FXImage(int arg0, int arg1) {
		super(arg0, arg1);
	}
	
	/*
	 * (non-Javadoc)
	 * @see processing.core.PImage#filter(int)
	 */
	public void filter(int arg0) {
		for (int filter:origFilters) {
			if (arg0==filter) {
				super.filter(arg0);
				return;
			}
		}
		switch (arg0) {
			default: System.out.println("Use filter(arg0,arg1) instead!");
		}
	}
	
	public void filter(int arg0, float arg1) {
		for (int filter:origFilters) {
			if (arg0==filter) {
				super.filter(arg0,arg1);
				return;
			}
		}
		
		switch (arg0) {
		case blurConvolute: System.out.println("blurConvolute "+arg1); SimpleConvolution.filter(this, (int)arg1); break;
		case blurHSimple: System.out.println("blurHSimple "+arg1); SimpleHBlur.filter(this, (int)arg1); break;
		case BLUR_SIMPLE: System.out.println("BLUR_SIMPLE "+arg1); SimpleBlur.filter(this, (int)arg1); break;
		case BLUR_SIMPLE_PREDIV: System.out.println("BLUR_SIMPLE_PREDIV "+arg1); SimpleBlurPrediv.filter(this, (int)arg1); break;
		case BLUR_SIMPLE_PREMULT: System.out.println("BLUR_SIMPLE_PREMULT "+arg1); SimpleBlurPreAlpha.filter(this, (int)arg1); break;
		case BLUR_SIMPLE_GAUSSIAN: System.out.println("BLUR_SIMPLE_GAUSSIAN "+arg1); SimpleBlurGaussian.filter(this, arg1); break;
		case BLUR_SIMPLE_GAUSSIAN_PREMULT: System.out.println("BLUR_SIMPLE_GAUSSIAN_PREMULT "+arg1); SimpleBlurGaussianPreAlpha.filter(this, arg1); break;
		case BLUR_SIMPLE_SLIDING: System.out.println("BLUR_SIMPLE_SLIDING "+arg1); SimpleBlurSliding.filter(this, (int)arg1); break;
		case BLUR_SIMPLE_SLIDING_PREDIV: System.out.println("BLUR_SIMPLE_SLIDING_PREDIV "+arg1); SimpleBlurSlidingPrediv.filter(this, (int)arg1); break;
		case BLUR_SIMPLE3_SLIDING_PREDIV_SUBPIXEL: System.out.println("BLUR_SIMPLE3_SLIDING_PREDIV_SUBPIXEL "+arg1); SimpleBlurSlidingPredivSubpix.filter(this, arg1); break;
		default: System.out.println("Use filter(arg0) instead!");
		}
	}
	
	public void filter(int arg0, float arg1, float arg2) {
		for (int filter:origFilters) {
			if (arg0==filter) {
				super.filter(arg0,arg1);
				return;
			}
		}
		
		switch (arg0) {
		case BLUR_SIMPLE: System.out.println("BLUR_SIMPLE "+arg1+", "+arg2); SimpleBlur.filter(this, (int)arg1, (int)arg2); break;
		case BLUR_SIMPLE_PREDIV: System.out.println("BLUR_SIMPLE_PREDIV "+arg1); SimpleBlurPrediv.filter(this, (int)arg1, (int)arg2); break;
		case BLUR_SIMPLE_PREMULT: System.out.println("BLUR_SIMPLE_PREMULT "+arg1); SimpleBlurPreAlpha.filter(this, (int)arg1, (int)arg2); break;
		case BLUR_SIMPLE_SLIDING: System.out.println("BLUR_SIMPLE2 "+arg1+", "+arg2); SimpleBlurSliding.filter(this, (int)arg1, (int)arg2); break;
		case BLUR_SIMPLE_SLIDING_PREDIV: System.out.println("BLUR_SIMPLE3 "+arg1+", "+arg2); SimpleBlurSlidingPrediv.filter(this, (int)arg1, (int)arg2); break;
		case BLUR_SIMPLE3_SLIDING_PREDIV_SUBPIXEL: System.out.println("BLUR_SIMPLE3_SUBPIXEL "+arg1+", "+arg2); SimpleBlurSlidingPredivSubpix.filter(this, arg1, arg2); break;
		case BLUR_SIMPLE_PATH: System.out.println("BLUR_SIMPLE_PATH distance: "+arg1+", angle: "+arg2); SimpleConvolutionPath.filter(this, arg1, arg2); break;
		case BLUR_SIMPLE_PATH_SUBPIXEL: System.out.println("BLUR_SIMPLE_PATH_SUBPIXEL distance: "+arg1+", angle: "+arg2); SimpleConvolutionPathSubpixel.filter(this, arg1, arg2); break;
		default: System.out.println("Use filter(arg0) instead!");
		}
	}
	
	public void filter(int arg0, Path path) {
		for (int filter:origFilters) {
			if (arg0==filter) {
				//super.filter(arg0,arg1);
				System.out.println("Cannot use classic filters with a Path");
				return;
			}
		}
		
		switch (arg0) {
		case BLUR_SIMPLE_PATH: System.out.println("BLUR_SIMPLE_PATH distance: "+path.size()); SimpleConvolutionPath.filter(this, path); break;
		case BLUR_SIMPLE_PATH_SUBPIXEL: System.out.println("BLUR_SIMPLE_PATH_SUBPIXEL distance: "+path.size()); SimpleConvolutionPathSubpixel.filter(this, path); break;
		default: System.out.println("Use filter(arg0) or filter(arg0,arg1) instead!");
		}
	}
	
	public void filter(int arg0, Path arg1, float arg2) {
		for (int filter:origFilters) {
			if (arg0==filter) {
				//super.filter(arg0,arg1);
				System.out.println("Cannot use classic filters with a Path");
				return;
			}
		}
		
		switch (arg0) {
		case BLUR_SIMPLE_PATH: System.out.println("BLUR_SIMPLE_PATH distance: "+arg1.size()); SimpleConvolutionPath.filter(this, arg1, arg2); break;
		case BLUR_SIMPLE_PATH_SUBPIXEL: System.out.println("BLUR_SIMPLE_PATH_SUBPIXEL distance: "+arg1.size()); SimpleConvolutionPathSubpixel.filter(this, arg1, arg2); break;
		default: System.out.println("Use filter(arg0) or filter(arg0,arg1) instead!");
		}
	}

}


