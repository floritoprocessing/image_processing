package blurring;

import processing.core.PImage;

/**
 * Simple blur using one horizontal and one vertical blur<br>
 * Using sliding window
 * Using float radius
 * @author mgraf
 *
 */
public class SimpleBlurSlidingPredivSubpix {
	
	public static void filter(PImage srcAndDst, float radius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurAndSwapAxes(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, radius);
		blurAndSwapAxes(dst,srcAndDst.pixels,srcAndDst.height, srcAndDst.width, radius);
	}
	
	public static void filter(PImage srcAndDst, float hRadius, float vRadius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurAndSwapAxes(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, hRadius);
		blurAndSwapAxes(dst,srcAndDst.pixels,srcAndDst.height, srcAndDst.width, vRadius);
	}
	
	private static void blurAndSwapAxes(int[] in, int[] out, int width, int height, float radius) {
		int radius0 = (int)radius;
		int radius1 = radius0+1;
		float interpolation = radius-radius0;
		int tableSize0 = 2*radius0+1;
		int tableSize1 = 2*radius1+1;
		
		/*
		 * Precalc all possible divisions:
		 * Make an array that has the total value as index
		 * Maximum total window value = tableSize * 255
		 */
		int divide0[] = new int[tableSize0*256];
		for (int i=0;i<divide0.length;i++) {
			divide0[i] = i/tableSize0;
		}
		int divide1[] = new int[tableSize1*256];
		for (int i=0;i<divide1.length;i++) {
			divide1[i] = i/tableSize1;
		}
		
		int wm1 = width-1;
		int indexX=0;
		int indexY=0;
		int rgb;
		int outIndex=0;
		int iNew0, iOld0, rgbNew0, rgbOld0;
		int iNew1, iOld1, rgbNew1, rgbOld1;
		int ta0=0, tr0=0, tg0=0, tb0=0;
		int ta1=0, tr1=0, tg1=0, tb1=0;
		for (int y=0;y<height;y++) {
			outIndex = y;
			ta0=0;
			tr0=0;
			tg0=0;
			tb0=0;
			ta1=0;
			tr1=0;
			tg1=0;
			tb1=0;
			
			/*
			 * Initial window
			 */
			for (int xo=-radius0;xo<=radius0;xo++) {
				indexX = xo;
				if (indexX<0) {
					indexX = 0;
				} else if (indexX>wm1) {
					indexX = wm1;
				}
				rgb = in[indexY+indexX];
				ta0 += (rgb>>24)&0xff;
				tr0 += (rgb>>16)&0xff;
				tg0 += (rgb>>8)&0xff;
				tb0 += rgb&0xff;
			}
			
			for (int xo=-radius1;xo<=radius1;xo++) {
				indexX = xo;
				if (indexX<0) {
					indexX = 0;
				} else if (indexX>wm1) {
					indexX = wm1;
				}
				rgb = in[indexY+indexX];
				ta1 += (rgb>>24)&0xff;
				tr1 += (rgb>>16)&0xff;
				tg1 += (rgb>>8)&0xff;
				tb1 += rgb&0xff;
			}
			
			
			for (int x=0;x<width;x++) {
				//out[outIndex] = ((int)(ta*kernFac))<<24 | ((int)(tr*kernFac))<<16 | ((int)(tg*kernFac))<<8 | ((int)(tb*kernFac));
				//out0 = divide0[ta0]<<24 | divide0[tr0]<<16 | divide0[tr0]<<8 | divide0[tb0];
				//out1 = divide1[ta1]<<24 | divide1[tr1]<<16 | divide1[tr1]<<8 | divide1[tb1];
				out[outIndex] = interpolate(divide0[ta0],divide1[ta1],interpolation)<<24
					| interpolate(divide0[tr0],divide1[tr1],interpolation)<<16
					| interpolate(divide0[tg0],divide1[tg1],interpolation)<<8
					| interpolate(divide0[tb0],divide1[tb1],interpolation);
				
				iNew0 = x+radius0+1;
				if (iNew0>wm1) iNew0=wm1;
				iOld0 = x-radius0;
				if (iOld0<0) iOld0=0;
				rgbNew0 = in[indexY+iNew0];
				rgbOld0 = in[indexY+iOld0];
				ta0 += ((rgbNew0>>24)&0xff)-((rgbOld0>>24)&0xff);
				tr0 += ((rgbNew0>>16)&0xff)-((rgbOld0>>16)&0xff);
				tg0 += ((rgbNew0>>8)&0xff)-((rgbOld0>>8)&0xff);
				tb0 += (rgbNew0&0xff)-(rgbOld0&0xff);
				
				iNew1 = x+radius1+1;
				if (iNew1>wm1) iNew1=wm1;
				iOld1 = x-radius1;
				if (iOld1<0) iOld1=0;
				rgbNew1 = in[indexY+iNew1];
				rgbOld1 = in[indexY+iOld1];
				ta1 += ((rgbNew1>>24)&0xff)-((rgbOld1>>24)&0xff);
				tr1 += ((rgbNew1>>16)&0xff)-((rgbOld1>>16)&0xff);
				tg1 += ((rgbNew1>>8)&0xff)-((rgbOld1>>8)&0xff);
				tb1 += (rgbNew1&0xff)-(rgbOld1&0xff);
				
				outIndex += height;
			}
			indexY += width;
		}	
	}
	
	private static int interpolate(int v0, int v1, float percentage) {
		return (int)(v0 + percentage*(v1-v0));
	}
	
}
