package blurring.dblur;

import java.util.Arrays;

import blurring.SimpleBlurGaussianPreAlpha;
import blurring.SimpleBlurSlidingPredivSubpix;
import processing.core.PApplet;
import processing.core.PGraphics;

public class DepthBlur extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"blurring.dblur.DepthBlur"});
	}
	
	private Rect[] rect;
	
	private float minZ = -1000;
	private float sharpMinZ = -150;
	private float sharpMaxZ = 0;
	private float maxZ = 150;
	
	private float minBlur = 0;
	private float maxBlur = 10;
	
	public void setup() {
		size(400,300,P3D);
		
		rect = new Rect[5];
		for (int i=0;i<rect.length;i++) {
			rect[i] = new Rect( random(width), random(height), random(minZ,maxZ), 
					random(50,80), random(50,80) , 
					color(random(64,92), random(64,92), random(64,92)),
					random(-1,1), random(-1,1), random(5,15)
					);
		}
		
		Arrays.sort(rect);
	}
	
	public void draw() {
		background(255);
		
		for (int i=0;i<rect.length;i++) {
			PGraphics pg = createGraphics(width, height, P3D);
			pg.beginDraw();
			pg.pushMatrix();
			pg.fill(rect[i].color);
			//pg.stroke(0);
			pg.noStroke();
			pg.translate(rect[i].x, rect[i].y, rect[i].z);
			pg.rotate(rect[i].rz);
			pg.rectMode(CORNER);
			pg.rect(-rect[i].width/2,-rect[i].height/2,rect[i].width,rect[i].height);
			pg.popMatrix();
			
			float z = rect[i].z;
			float r = 0;
			if (z<sharpMinZ) {
				r = interpolate(z,minZ,sharpMinZ,maxBlur,minBlur,true);
			} else if (z>sharpMaxZ) {
				r = interpolate(z,sharpMaxZ,maxZ,minBlur,maxBlur,true);
			}
			if (r!=0) SimpleBlurGaussianPreAlpha.filter(pg, r);
			//if (r!=0) SimpleBlurSlidingPredivSubpix.filter(pg, r);
			pg.endDraw();
			
			
			
			blend(pg,0,0,width,height,0,0,width,height,NORMAL);
		}
		
		for (int i=0;i<rect.length;i++) {
			rect[i].move();
			if (rect[i].z>maxZ) rect[i].z = minZ;
		}
		Arrays.sort(rect);
	}

	private float interpolate(float val, float v0, float v1, float out0, float out1, boolean clamp) {
		float p = (val-v0)/(v1-v0);
		if (clamp) {
			p = Math.max(0,Math.min(1,p));
		}
		return out0 + p*(out1-out0);
	}
	
}
