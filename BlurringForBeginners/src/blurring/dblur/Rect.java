package blurring.dblur;

public class Rect implements Comparable {
	
	public float x, y, z;
	public float width, height;
	public float rz;
	public int color;
	public float xm, ym, zm;
	
	public Rect(float x, float y, float z, float width, float height, int color, float xm, float ym, float zm) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.width = width;
		this.height = height;
		this.color = color;
		this.xm = xm;
		this.ym = ym;
		this.zm = zm;
		rz = (float)(Math.PI*2*Math.random());
	}

	public void move() {
		x += xm;
		y += ym;
		z += zm;
	}
	
	public int compareTo(Object arg0) {
		Rect other = (Rect)arg0;
		if (other.z<z) {
			return 1;
		} else if (other.z>z) {
			return -1;
		} else {
			return 0;
		}
	}

}
