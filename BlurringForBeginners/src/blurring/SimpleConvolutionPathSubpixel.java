package blurring;

import java.awt.geom.Point2D;

import processing.core.PImage;

public class SimpleConvolutionPathSubpixel {

	public static boolean DEBUG;
	
	public static void filter(PImage srcAndDst, float distance, float angle) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		
		int amount = (int)Math.ceil(distance);
		Point2D.Float[] points = new Point2D.Float[amount];
		float cos = (float)Math.cos(angle);
		float sin = (float)Math.sin(angle);
		for (int i=0;i<amount;i++) {
			float r = distance*i/(amount-1) - distance/2;
			//r *= 5;
			points[i] = new Point2D.Float(r*cos,r*sin); 
		}
		Path path = new Path(points);
		
		convolve(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, path, 1);//radius);
		srcAndDst.pixels = dst;
	}
	
	public static void filter(PImage srcAndDst, Path path, float gain) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		convolve(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, path, gain);//radius);
		srcAndDst.pixels = dst;
	}
	
	public static void filter(PImage srcAndDst, Path path) {
		filter(srcAndDst, path, 1);
	}
	
	public static void convolve(int[] in, int[] out, int width, int height, Path path, float gain) {
		
		int wm1 = width-1;
		int hm1 = height-1;
		int rowIndex=0;
		int col00, col01, col10, col11;
		float px0, px1, py0, py1;
		float p00, p01, p10, p11;
		int rgb=0;
		int lastPercentage=0;
		for (int y=0;y<height;y++) {
			rowIndex = y*width;
			
			for (int x=0;x<width;x++) {
				int ta=0, tr=0, tg=0, tb=0;
				int total = 0;
				
				for (int i=0;i<path.size();i++) {
					
					float xo = path.getPoint(i).x;
					float yo = path.getPoint(i).y;
					
					int x0 = (int)(x+xo);
					int y0 = (int)(y+yo);
					int x1 = x0+1;
					int y1 = y0+1;
					
					/*if (x0<0) {
						x0 = 0;
						x1 = 1;
						xo += (0-x0);
					}*/
					
					if (x0<0||x1>wm1||y0<0||y1>hm1) {
						//rgb = in[y*width+x];
						//rgb=0;
					} else {
						int sourceIndex = y0*width + x0;
						col00 = in[sourceIndex];
						col01 = in[sourceIndex+width];
						col10 = in[sourceIndex+1];
						col11 = in[sourceIndex+width+1];
						px1 = (x+xo-x0);
						px0 = 1-px1;
						py1 = (y+yo-y0);
						py0 = 1-py1;
						p00 = px0*py0;
						p01 = px0*py1;
						p10 = px1*py0;
						p11 = px1*py1;
						rgb = mixColor(col00,col01,col10,col11,p00,p01,p10,p11);
						ta += (rgb>>24)&0xff;
						tr += (rgb>>16)&0xff;
						tg += (rgb>>8)&0xff;
						tb += rgb&0xff;
						total ++;
					}
					
					
					
				}
				
				if (total>0) {
					ta /= total;
					tr *= gain/total;
					tg *= gain/total;
					tb *= gain/total;
					if (tr>255) tr=255;
					if (tg>255) tg=255;
					if (tb>255) tb=255;
					out[rowIndex+x] = ta<<24 | tr<<16 | tg<<8 | tb;
				} else {
					out[rowIndex+x] = in[rowIndex+x];
				}
			}
			
			if (DEBUG) {
				int percentage = (100*y/height);
				if (percentage%5==0 && percentage!=lastPercentage) {
					System.out.print(percentage+"..");
					lastPercentage=percentage;
				}
			}
		}
		System.out.println();
		
	}

	private static int mixColor(int col00, int col01, int col10, int col11, float p00, float p01, float p10, float p11) {
		int alpha = (int)(alpha(col00)*p00 + alpha(col01)*p01 + alpha(col10)*p10 + alpha(col11)*p11);
		int red = (int)(red(col00)*p00 + red(col01)*p01 + red(col10)*p10 + red(col11)*p11);
		int green = (int)(green(col00)*p00 + green(col01)*p01 + green(col10)*p10 + green(col11)*p11);
		int blue = (int)(blue(col00)*p00 + blue(col01)*p01 + blue(col10)*p10 + blue(col11)*p11);
		return alpha<<24|red<<16|green<<8|blue;
	}
	
	private static int alpha(int v) {
		return v>>24&0xff;
	}
	
	private static int red(int v) {
		return v>>16&0xff;
	}
	
	private static int green(int v) {
		return v>>8&0xff;
	}
	
	private static int blue(int v) {
		return v&0xff;
	}
	
}
