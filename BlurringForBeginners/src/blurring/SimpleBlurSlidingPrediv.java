package blurring;

import processing.core.PImage;

/**
 * Simple blur using one horizontal and one vertical blur<br>
 * Using sliding window
 * @author mgraf
 *
 */
public class SimpleBlurSlidingPrediv {
	
	public static void filter(PImage srcAndDst, int radius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurAndSwapAxes(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, radius);
		blurAndSwapAxes(dst,srcAndDst.pixels,srcAndDst.height, srcAndDst.width, radius);
	}
	
	public static void filter(PImage srcAndDst, int hRadius, int vRadius) {
		srcAndDst.loadPixels();
		int[] dst = new int[srcAndDst.pixels.length];
		blurAndSwapAxes(srcAndDst.pixels,dst,srcAndDst.width, srcAndDst.height, hRadius);
		blurAndSwapAxes(dst,srcAndDst.pixels,srcAndDst.height, srcAndDst.width, vRadius);
	}
	
	private static void blurAndSwapAxes(int[] in, int[] out, int width, int height, int radius) {
		int tableSize = 2*radius+1;
		/*
		 * Precalc all possible divisions:
		 * Make an array that has the total value as index
		 * Maximum total window value = tableSize * 255
		 */
		int divide[] = new int[tableSize*256];
		for (int i=0;i<divide.length;i++) {
			divide[i] = i/tableSize;
		}
		int wm1 = width-1;
		int indexX=0;
		int indexY=0;
		int rgb;
		int outIndex=0;
		int iNew, iOld, rgbNew, rgbOld;
		int ta=0, tr=0, tg=0, tb=0;
		for (int y=0;y<height;y++) {
			outIndex = y;
			ta=0;
			tr=0;
			tg=0;
			tb=0;
			
			/*
			 * Initial window
			 */
			for (int xo=-radius;xo<=radius;xo++) {
				indexX = xo;
				if (indexX<0) {
					indexX = 0;
				} else if (indexX>wm1) {
					indexX = wm1;
				}
				rgb = in[indexY+indexX];
				ta += (rgb>>24)&0xff;
				tr += (rgb>>16)&0xff;
				tg += (rgb>>8)&0xff;
				tb += rgb&0xff;
			}
			
			
			for (int x=0;x<width;x++) {
				//out[outIndex] = ((int)(ta*kernFac))<<24 | ((int)(tr*kernFac))<<16 | ((int)(tg*kernFac))<<8 | ((int)(tb*kernFac));
				out[outIndex] = divide[ta]<<24 | divide[tr]<<16 | divide[tg]<<8 | divide[tb];
				
				iNew = x+radius+1;
				if (iNew>wm1) iNew=wm1;
				iOld = x-radius;
				if (iOld<0) iOld=0;
				
				rgbNew = in[indexY+iNew];
				rgbOld = in[indexY+iOld];
				
				ta += ((rgbNew>>24)&0xff)-((rgbOld>>24)&0xff);
				tr += ((rgbNew>>16)&0xff)-((rgbOld>>16)&0xff);
				tg += ((rgbNew>>8)&0xff)-((rgbOld>>8)&0xff);
				tb += (rgbNew&0xff)-(rgbOld&0xff);
				
				outIndex += height;
			}
			indexY += width;
		}	
	}
	
}
