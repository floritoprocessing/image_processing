//PIXEL STRETCHING

int MIN_LENGTH = 150;
int MAX_LENGTH = 400;

boolean showOriginal = false;

PImage img;
boolean[] pixelTaken;
int amountPixelsTaken;
int maxPixels;

PImage overlay;


void setup() {

  size(1300, 900);

  img = loadImage("cat1.jpg");
  //img = loadImage("landscape.jpg");
  //img = loadImage("bambi.png");
  img.loadPixels();

  println("Creating image of "+img.width+"x"+img.height);
  overlay = new PImage(img.width, img.height);
  overlay.format = ARGB;

  maxPixels = img.pixels.length;
  println("maxPixel="+maxPixels);
  amountPixelsTaken = 0;
  pixelTaken = new boolean[maxPixels];
}

void mousePressed() {
  showOriginal = !showOriginal;
}

void draw() {

  background(0);

  translate(width/2-img.width/2, height/2-img.height/2);
  if (showOriginal)
    image(img, 0, 0);

  image(overlay, 0, 0);
  overlay.loadPixels();

  for (int i=0; i<100; i++) {
    if (amountPixelsTaken < maxPixels) {

      int index = findNotTaken();

      boolean horizontal = random(1.0)<0.5;
      int lineLength = (int)random(MIN_LENGTH, MAX_LENGTH+1);
      int cx = index%img.width;
      int cy = (int)floor(index/img.width);

      if (horizontal) {
        int cx0 = max(0, cx-lineLength/2);
        int cx1 = min(img.width-1, cx0+lineLength);

        for (int x=cx; x>=cx0; x--) {
          if (!drawIfPixelNotTaken(x,cy,index)) break;
        }
        for (int x=cx; x<cx1; x++) {
          if (!drawIfPixelNotTaken(x,cy,index)) break;
        }
        
      } else {
        int cy0 = max(0, cy-lineLength/2);
        int cy1 = min(img.height-1, cy0+lineLength);

        for (int y=cy; y>=cy0; y--) {
          if (!drawIfPixelNotTaken(cx,y,index)) break;
        }
        for (int y=cy; y<cy1; y++) {
          if (!drawIfPixelNotTaken(cx,y,index)) break;
        }
        
      }

      //pixelTaken[index] = true;
      //overlay.pixels[index] = color(0);
      overlay.updatePixels();
    } else {
      println("DONE!");
    }
  }
}



boolean drawIfPixelNotTaken(int x, int y, int index) {
  int theIndex = y*img.width + x;
  if (pixelTaken[theIndex]==false) {
    overlay.pixels[theIndex] = img.pixels[index];
    pixelTaken[theIndex]=true;
    return true;
  } else {
    return false;
  }
}

int findNotTaken() {
  int index;
  do {
    index = (int)random(maxPixels);
  } while (pixelTaken[index]==true);
  return index;
}
