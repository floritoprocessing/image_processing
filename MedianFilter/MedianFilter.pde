
PImage img;
int MEDIAN_SIZE=1;
PFont font;

void setup() {
  size(640,480,JAVA2D);
  font = loadFont("Arial-BoldMT-12.vlw");
  textFont(font);
  img=loadImage("noisy1.jpg");
  img.loadPixels();
  for (int i=0;i<img.pixels.length;i++) img.pixels[i] = color(brightness(img.pixels[i]));
}

void mousePressed() {
  medianFilter(img);
}

void draw() {
  image(img,0,0);
  
  int ti = millis();
  medianFilter(img);
  ti = millis()-ti;
  
  
  println(MEDIAN_SIZE);
  
  fill(0,0,0,192);
  text("Frame: "+nf(frameCount,3)+", Median size: "+MEDIAN_SIZE+", Time: "+ti+" ms",11,21);
  fill(255,0,0);
  text("Frame: "+nf(frameCount,3)+", Median size: "+MEDIAN_SIZE+", Time: "+ti+" ms",10,20);
  
  MEDIAN_SIZE = (frameCount/10)+1;
  
  int nfrm = 1; //2
  String bspth = "V:\\Processing visual index new\\";
  String className = getClass().getName();
  //if (frameCount%nfrm==0) {
  //  saveFrame(bspth+className+"\\"+className+"_######.bmp");
  //  int savedFrames = frameCount/nfrm;
  //  int frames = savedFrames%25;
  //  int seconds = (savedFrames/25);
  //  println("saved frame "+(frameCount/nfrm)+" - movie time: "+nf(seconds,2)+":"+nf(frames,2));
  //}
}

void medianFilter(PImage img) {
  img.loadPixels();
  for (int x=0;x<img.width;x++) {
    for (int y=0;y<img.height;y++) {

      float brightArray[] = new float[(2*MEDIAN_SIZE+1)*(2*MEDIAN_SIZE+1)];
      int nrOfPixels=0;
      for (int xo=-MEDIAN_SIZE;xo<=MEDIAN_SIZE;xo++) {
        for (int yo=-MEDIAN_SIZE;yo<=MEDIAN_SIZE;yo++) {
          int nx=x+xo;
          int ny=y+yo;
          if (nx>=0&&nx<img.width&&ny>=0&&ny<img.height) {
            brightArray[nrOfPixels]=red(img.get(nx,ny));
            nrOfPixels++;
          }
        }
      }

      float sortArray[] = new float[nrOfPixels];
      for (int i=0;i<nrOfPixels;i++) sortArray[i]=brightArray[i];

      boolean swapped=true;
      int n=sortArray.length-1;
      while(swapped&&n>0) {
        swapped=false;
        for (int i=0;i<n;i++) {
          if (sortArray[i]>sortArray[i+1]) {
            float tmp=sortArray[i];
            sortArray[i]=sortArray[i+1];
            sortArray[i+1]=tmp;
            swapped=true;
          }
        }
        n--;
      }

      img.set(x,y,color(sortArray[sortArray.length/2]));

    }
  }
  print("ok.");
}
