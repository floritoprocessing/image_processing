
PImage img;
int MEDIAN_SIZE=1;
PFont font;

void setup() {
  size(640,480,JAVA2D);
  font = loadFont("Arial-BoldMT-12.vlw");
  textFont(font);
  img=loadImage("noisy1.jpg");
  img.loadPixels();
  for (int i=0;i<img.pixels.length;i++) {
    img.pixels[i] = color(brightness(img.pixels[i]));
  }
}

void mousePressed() {
  medianFilter(img, MEDIAN_SIZE);
}

void draw() {
  image(img,0,0);
  int ti = millis();
  medianFilter(img, MEDIAN_SIZE);
  ti = millis()-ti;
  
  println("SIZE: "+MEDIAN_SIZE+", count "+(1+(frameCount%10))+", time: "+ti);
  fill(0,0,0,192);
  text("Frame: "+nf(frameCount,3)+", Median size: "+MEDIAN_SIZE+", Time: "+ti+" ms",11,21);
  fill(255,0,0);
  text("Frame: "+nf(frameCount,3)+", Median size: "+MEDIAN_SIZE+", Time: "+ti+" ms",10,20);
  
  MEDIAN_SIZE = (frameCount/10)+1;
}

void medianFilter(PImage img, int side) {
  int[] out = new int[img.pixels.length];
  int len = 2*side+1;
  int lenSq = len*len;
  int halfArray = lenSq/2;
  
  /*
  *  create window
  */
  int[] windowIndex = new int[lenSq];
  int[] xo = new int[lenSq];
  int[] yo = new int[lenSq];
  int fi=0;
  for (int y=-side;y<=side;y++) {
    for (int x=-side;x<=side;x++) {
      windowIndex[fi] = y*img.width + x;
      xo[fi] = x;
      yo[fi] = y;
      fi++;
    }
  }
  
  /*
  *  Slide window
  */
  int srcIndex = 0;
  int dstIndex = 0;
  int[] window = new int[lenSq];
  for (int y=0;y<img.height;y++) {
    for (int x=0;x<img.width;x++) {
      
      for (int i=0;i<windowIndex.length;i++) {
        
        srcIndex = dstIndex + windowIndex[i];
        if (x+xo[i]<0) {
          srcIndex-=xo[i];
        } else if (x+xo[i]>width-1) {
          srcIndex-=xo[i];
        }
        if (y+yo[i]<0) {
          srcIndex-=yo[i]*width;
        } else if (y+yo[i]>height-1) {
          srcIndex-=yo[i]*width;
        }
        
        //try {
          window[i] = (img.pixels[srcIndex]&0xff);
        //} catch (ArrayIndexOutOfBoundsException e) {
        /// e.printStackTrace();
        //  println(x+"/"+y+", offset "+xo[i]+"/"+yo[i]+" index "+i+" -> "+srcIndex);
        //  System.exit(0);
        //}
      }
      
      java.util.Arrays.sort(window);
      out[dstIndex] = window[halfArray]<<16 | window[halfArray]<<8 | window[halfArray];
      
      dstIndex++;
    }
  }
  
  img.pixels = out;
  img.updatePixels();
  
  print("ok.");
}
