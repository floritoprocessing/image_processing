PImage img;
Smudger[] smudger; int nrOfSmudgers, nrOfMaxSmudgers;

void setup() {
  // setup screen and load image
  size(640,480); img=loadImage("image.jpg"); image(img,0,0); //framerate(25);
  // setup smudgers
  nrOfSmudgers=500; smudger=new Smudger[nrOfSmudgers];
  for (int i=0;i<nrOfSmudgers;i++) { smudger[i]=new Smudger(); }
}

void draw() {
  image(img,0,0);
  if (mousePressed) {
    int i=0;
    while(smudger[i].active&&i<nrOfSmudgers-1) {i++;}
    if (i!=nrOfSmudgers-1) {
      smudger[i].init(mouseX,mouseY,20,1);
      println(i);
    }
  }
  for (int i=0;i<nrOfSmudgers;i++) {
    smudger[i].update();
  }
}

void smudge(int bx, int by, int r, float s) {  // x/y position, radius, strength
  int sx,sy, d2,r2, av_col;
  
  // get color through getting average from img under circle:
  int rr=0,gg=0,bb=0;
  int nrOfSamples=0;
  for (int x=-r;x<=r;x++) { for (int y=-r;y<=r;y++) {
    d2=x*x+y*y; r2=r*r;     
    if (d2<r2) {
      sx=bx+x; sy=by+y;
      rr+=(img.get(sx,sy)>>16)&255; gg+=(img.get(sx,sy)>>8)&255; bb+=(img.get(sx,sy))&255;
      nrOfSamples++;
    }
  } }
  av_col=rr/nrOfSamples<<16 | gg/nrOfSamples<<8 | bb/nrOfSamples;
  
  // get color through looking under mouse:  av_col=img.get(bx,by);
  for (int x=-r;x<=r;x++) { for (int y=-r;y<=r;y++) {
    d2=x*x+y*y; r2=r*r;     
    if (d2<r2) {
      sx=bx+x; sy=by+y;
      float p=pow(1.0-abs(d2/(float)r2),2);
      int c=cmix(av_col,get(sx,sy),s*p);
      set(sx,sy,c);            
    }
  } }
}

class Smudger {
  int x,y;  // position
  float r,s;  // radius, strength
  boolean active;
  float damp=0.996;
  
  Smudger() {
    active=false;
  }
  
  void init(int ix, int iy, int ir, float is) {
    x=ix; y=iy; r=ir; s=is; active=true;
  }
  
  void update() {
    if (active) {
      r*=damp; s*=damp;
      if (r>1) {
        smudge(x,y,int(r),s);  // x/y position, radius, strength
      } else {active=false;}    
    }
  }
}

int cmix(color c1,color c2, float p1) {
  float p2=1-p1;
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  return int(255<<24|r<<16|g<<8|b);
}
