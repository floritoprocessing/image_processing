int nrOfParticles=10000;


void setup() {
  img=loadImage("Vermeer800x600.jpg");
  size(800,600); colorMode(RGB,255.0);
  initParticles(nrOfParticles,width,height);
  //image(img,0,0,width,height);
  background(255);
}

void draw() {
  //background(255);
  updateParticlePosition(nrOfParticles);
  drawParticles(nrOfParticles);
}

PImage img;

// begin
// LIBRARY PARTICLE
// HOLDS:
//
// -  class Particle(width,height)
//    position: x,m    position limit: xMax, yMax  [0..1]
//    movement: xm,ym
//    bounce: bouncyness in respect to border (true/false)
//    col: color of particle
// -  array particle with nrOfParticle items
// -  void initParticlePosition(arraylength,width,height)
// -  void updateParticles(arraylength)
// -  void drawParticles(arraylength)

Particle[] particle=new Particle[nrOfParticles];

void initParticles(int _n,int _w, int _h) { for (int i=0;i<_n;i++) { particle[i]=new Particle(_w,_h); } }
void updateParticlePosition(int _n) { for (int i=0;i<_n;i++) { particle[i].update(); } }
void drawParticles(int _n) { for (int i=0;i<_n;i++) { particle[i].drawMe(); } }

class Particle {
  float sScale;  // scaleFac from 0..xMax/yMax to screensize
  float x=0, y=0;
  float xMax=1, yMax=1;
  float xm=0, ym=0;
  float rd=0, r=0;
  boolean bounce=false; // determins the particles behaviour on the edge
  float pressure=0.0;
  float pressAdd=0.001;
  float colDiff=0;
  color col=color(random(255),random(255),random(255));
  
  // INIT PARTICLE
  Particle(int _w,int _h) {
    sScale=_w>_h?_w:_h;
    if (_w>_h) { xMax=1.0; yMax=_h/(float)_w; } else { yMax=1.0; xMax=_w/(float)_h; }
    x=random(xMax); y=random(yMax);
    xm=random(0.0005,0.002); ym=random(0.0005,0.002);
    if (random(1)<0.5) {xm=-xm;} if (random(1)<0.5) {ym=-ym;}
    r=sqrt(sq(xm)+sq(ym)); rd=atan2(ym,xm);
  }
  
  // UPDATE POSITION, MOVEMENT OF PARTICLE
  void update() {
    
    rd+=(1-colDiff)*random(-1,1);
    xm=r*cos(rd); ym=r*sin(rd);
    x+=xm; y+=ym; 
    if (!bounce) {
      if (x<0) {x+=xMax;} if (x>=xMax) {x-=xMax;}
      if (y<0) {y+=yMax;} if (y>=yMax) {y-=yMax;}
    } else {
      if (x<0) {xm*=-1; x=-1*x; } if (x>=xMax) {xm*=-1; x=xMax-1*(x-xMax); }
      if (y<0) {ym*=-1; y=-1*y; } if (y>=yMax) {ym*=-1; y=yMax-1*(y-yMax); }
    }
    
    // get color under pixel:
    color imgCol=img.get(int(x*sScale),int(y*sScale));
    // check color difference:  [0..1]
    colDiff=colorDifference2(imgCol,col);
    pressure+=(0.5-colDiff)*pressAdd;
    pressure=constrain(pressure,0,0.5);
  }
  
  // DRAW THE PARTICLE
  void drawMe() {
    sSet(int(x*sScale),int(y*sScale),col,pressure);
  }
}

// LIBRARY PARTICLE 
// end






void sSet(int x, int y, int c, float p) {
  int bg=get(x,y);
  set(x,y,mix(c,bg,p));
}

color mix(int a, int b, float p) {
  float q=1.0-p;
  int rr=int(ch_red(a)*p+ch_red(b)*q);
  int gg=int(ch_grn(a)*p+ch_grn(b)*q);
  int bb=int(ch_blu(a)*p+ch_blu(b)*q);
  return color(rr,gg,bb);
}

float colorDifference(color a, color b) {
  float dr=abs(red(a)-red(b));
  float dg=abs(green(a)-green(b));
  float db=abs(blue(a)-blue(b));
  return ((dr+dg+db)/765.0);
}

float colorDifference2(color a, color b) {
  colorMode(RGB,255.0);
  float dr=abs(red(a)-red(b));      //0..1 for lo/high difference
  float dg=abs(green(a)-green(b));
  float db=abs(blue(a)-blue(b));
  float d=dr*dg*db/16581375.0;
  return d;
}

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }
